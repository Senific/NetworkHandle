﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Windows.Forms;
using System.Diagnostics;

namespace NetworkHandle
{
    [Serializable]
    public delegate void HashtableEventHandler(object sender, HashtableEventArgs args);

    public delegate bool Approval(HashtableEventArgs args);

    public class ObservableHashtable : Hashtable
    {
        public event HashtableEventHandler BeforeAddItem;
        public event HashtableEventHandler AfterAddItem;
        public event HashtableEventHandler BeforeChangeItem;
        public event HashtableEventHandler AfterChangeItem;

        protected virtual bool OnBeforeAdd(HashtableEventArgs e)
        {
            HashtableEventHandler beforeAddItem = BeforeAddItem;
            if (beforeAddItem != null)
            {
                beforeAddItem(this, e);
                return (e.KeepChanges);
            }

            return (true);
        }

        protected virtual void OnAfterAdd(HashtableEventArgs e)
        {
            HashtableEventHandler afterAddItem = AfterAddItem;
            if (afterAddItem != null)
            {
                afterAddItem(this, e);
            }
        }

        protected virtual bool OnBeforeChange(HashtableEventArgs e)
        {
            HashtableEventHandler beforeChangeItem = BeforeChangeItem;
            if (beforeChangeItem != null)
            {
                beforeChangeItem(this, e);
                return (e.KeepChanges);
            }

            return (true);
        }

        protected virtual void OnAfterChange(HashtableEventArgs e)
        {
            HashtableEventHandler afterChangeItem = AfterChangeItem;
            if (afterChangeItem != null)
            {
                afterChangeItem(this, e);
            }
        }

        public override void Add(object key, object value)
        {
            HashtableEventArgs hashArgs =
                new HashtableEventArgs(key, value);
            if (OnBeforeAdd(hashArgs))
            {
                base.Add(key, value);
            }
            else
            {
                Debug.WriteLine("Addition of key/value cannot be performed");
            }

            OnAfterAdd(hashArgs);
        }

        public override object this[object key]
        {
            get
            {
                return (base[key]);
            }
            set
            {
                // See if this key is there to be changed; if not, add it.
                if (base.ContainsKey(key))
                {
                    HashtableEventArgs hashArgs = new HashtableEventArgs(key, value);
                    if (OnBeforeChange(hashArgs))
                    {
                        base[key] = value;
                    }
                    else
                    {
                        Debug.WriteLine("Change of value cannot be performed");
                    }

                    OnAfterChange(hashArgs);
                }
                else
                {
                    Debug.WriteLine("Item did not exist, adding");
                    Add(key, value);
                }
            }
        }
    }


    // The observer object that will observe a registered
    // ObservableHashtable object
    public class HashtableObserver
    {
        public HashtableObserver() { }

        // Set up delegate/events for approving an addition or change.
        public delegate bool Approval(HashtableEventArgs args);
        public event Approval ApproveAdd;
        public event Approval ApproveChange;

        public void Register(ObservableHashtable hashtable)
        {
            // Hook up to the ObservableHashTable instance events.
            hashtable.BeforeAddItem +=
                new HashtableEventHandler(BeforeAddListener);
            hashtable.AfterAddItem +=
                new HashtableEventHandler(AfterAddListener);
            hashtable.BeforeChangeItem +=
                new HashtableEventHandler(BeforeChangeListener);
            hashtable.AfterChangeItem +=
                new HashtableEventHandler(AfterChangeListener);
        }

        public void Unregister(ObservableHashtable hashtable)
        {
            // Unhook from the ObservableHashTable instance events.
            hashtable.BeforeAddItem -=
                new HashtableEventHandler(BeforeAddListener);
            hashtable.AfterAddItem -=
                new HashtableEventHandler(AfterAddListener);
            hashtable.BeforeChangeItem -=
                new HashtableEventHandler(BeforeChangeListener);
            hashtable.AfterChangeItem -=
                new HashtableEventHandler(AfterChangeListener);
        }

        private void CheckApproval(Approval approval,
                            HashtableEventArgs args)
        {
            // Check everyone who wants to approve.
            foreach (Approval approvalInstance in approval.GetInvocationList())
            {
                if (!approvalInstance(args))
                {
                    // If any of the concerned parties
                    // refuse, then no add. Adds by default.
                    args.KeepChanges = false;
                    break;
                }
            }
        }
        public void BeforeAddListener(object sender, HashtableEventArgs args)
        {
            // See if anyone is hooked up for approval.
            Approval approveAdd = ApproveAdd;
            if (approveAdd != null)
            {
                CheckApproval(approveAdd, args);
            }

            Debug.WriteLine("[NOTIFY] Before Add…: Add Approval = " +
                                            args.KeepChanges.ToString());
        }

        public void AfterAddListener(object sender, HashtableEventArgs args)
        {
            Debug.WriteLine("[NOTIFY] …After Add: Item approved for adding: " +
                                args.KeepChanges.ToString());
        }

        public void BeforeChangeListener(object sender, HashtableEventArgs args)
        {
            // See if anyone is hooked up for approval.
            Approval approveChange = ApproveChange;
            if (approveChange != null)
            {
                CheckApproval(approveChange, args);
            }

            Debug.WriteLine("[NOTIFY] Before Change…: Change Approval = " +
                                args.KeepChanges.ToString());
        }

        public void AfterChangeListener(object sender, HashtableEventArgs args)
        {
            Debug.WriteLine("[NOTIFY] …After Change: Item approved for change: " +
                                args.KeepChanges.ToString());
        }
    }

    // Event arguments for ObservableHashtable
    public class HashtableEventArgs : EventArgs
    {
        public HashtableEventArgs(object key, object value)
        {
            this.key = key;
            this.value = value;
        }

        private object key = null;
        private object value = null;
        private bool keepChanges = true;

        public bool KeepChanges
        {
            get { return (keepChanges); }
            set { keepChanges = value; }
        }

        public object Key
        {
            get { return (key); }
        }

        public object Value
        {
            get { return (value); }
        }
    }

}