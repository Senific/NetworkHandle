﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Windows .Forms ;
using System.Collections; 

namespace NetworkHandle
{
    public class SendAndReceive
    {

        private Messages.AuthenticationMessage Auth_Msg;
        private Messages.TypeAndSizeMessage TPNSZ_Msg;
        private Messages.ChatMessage Cht_Msg;
        private Messages.StatusMessage Stst_Msg; 

        public int SendPacket(TcpClient cl, byte[] TypeAndSizeBuffer, byte[] buffer)
        {
            int total = 0;
            int size = buffer.Length;
            int dataleft = size;
            int sent;

            cl.Client.Send(TypeAndSizeBuffer);

            while (total < size)
            {
                sent = cl.Client.Send(buffer, total, dataleft, SocketFlags.None);
                total += sent;
                dataleft -= sent;
            }

            return total;
        }
        public byte[] ReceivePacket(Client cl, int packet_size)
        {
            int total = 0;
            int dataleft = packet_size;
            int recv = 0;


            byte[] data = new byte[packet_size];

            while (total < packet_size)
            {
                try
                {
                    recv = cl.client.Client.Receive(data, total, dataleft, 0);

                }
                catch (SocketException ex)
                {
                    cl.Dispose(); 
                    break; 
                } 
                if (recv == 0)
                {
                    MessageBox.Show("connection recevied zero bytes. , so  if rcv == 0  condition was excuted."); 
                }
                total += recv;
                dataleft -= recv;
            }
            return data;
        }


        public Messages.TypeAndSizeMessage ReceiveUpCommingMessageTypeAndSize(Client cl)
        {
            byte[] buffer;

            buffer = ReceivePacket(cl, Messages.BinaryDataTypeSizes.int_DataTypeSize * 2);
            TPNSZ_Msg = new NetworkHandle.Messages.TypeAndSizeMessage(buffer);


            return TPNSZ_Msg;

        }

        public Messages.AuthenticationMessage Receive_Authentication_Message(Client cl)
        {

            byte[] buffer;


            buffer = ReceivePacket(cl, Messages.BinaryDataTypeSizes.int_DataTypeSize * 2);
            TPNSZ_Msg = new NetworkHandle.Messages.TypeAndSizeMessage(buffer);
           

            buffer = ReceivePacket(cl, TPNSZ_Msg.Packet_Size);
            Auth_Msg = new NetworkHandle.Messages.AuthenticationMessage(buffer);

            return Auth_Msg;

        }
        public void Send_Authentication_Message(TcpClient cl, string MyUserName, Messages.MsgTypes msgType, MyStatus my_status)
        {
            byte[] buffer;
            Auth_Msg = new NetworkHandle.Messages.AuthenticationMessage();
            Auth_Msg.UserName = MyUserName;
            Auth_Msg.Status = my_status;

            buffer = Auth_Msg.GetByte();

            TPNSZ_Msg = new NetworkHandle.Messages.TypeAndSizeMessage();
            TPNSZ_Msg.Packet_Size = buffer.Length;
            TPNSZ_Msg.MsgType = msgType;



            SendPacket(cl, TPNSZ_Msg.GetByte(), buffer);

        }

        public void Send_Chat_Message(TcpClient cl, int ID, string msg)
        {
            byte[] buffer;

            Cht_Msg = new NetworkHandle.Messages.ChatMessage();
            Cht_Msg.ID = ID;
            Cht_Msg.Msg = msg;


            buffer = Cht_Msg.GetByte();

            TPNSZ_Msg = new NetworkHandle.Messages.TypeAndSizeMessage();
            TPNSZ_Msg.Packet_Size = buffer.Length;
            TPNSZ_Msg.MsgType = NetworkHandle.Messages.MsgTypes.Chat;

            SendPacket(cl, TPNSZ_Msg.GetByte(), buffer);

        }
        public Messages.ChatMessage  ReceiveChatMessage(Client  cl , int Pkt_size  ) 
        {
            byte[] buffer;
            buffer = ReceivePacket(cl , Pkt_size );
            
            return  new NetworkHandle.Messages.ChatMessage(buffer);

        }

        public void Send_Status_Message(TcpClient cl, MyStatus status)
        {
            byte[] buffer;

            Stst_Msg = new NetworkHandle.Messages.StatusMessage();
            Stst_Msg.Status = status;
            buffer = Stst_Msg.GetByte();

            TPNSZ_Msg = new NetworkHandle.Messages.TypeAndSizeMessage();
            TPNSZ_Msg.Packet_Size = buffer.Length;
            TPNSZ_Msg.MsgType = NetworkHandle.Messages.MsgTypes.Status;

            SendPacket(cl, TPNSZ_Msg.GetByte(), buffer); 
        }
        public NetworkHandle.Messages.StatusMessage Recive_Status_Message(Client cl, int Pkt_size)
        {
            byte[] buffer;
            buffer = ReceivePacket(cl, Pkt_size);
            return new NetworkHandle.Messages.StatusMessage(buffer);  
        }

        public void Send_Status_Message_ForAllConnections(MyStatus status)
        {
            byte[] buffer;

            Stst_Msg = new NetworkHandle.Messages.StatusMessage();
            Stst_Msg.Status = status;
            buffer = Stst_Msg.GetByte();

            TPNSZ_Msg = new NetworkHandle.Messages.TypeAndSizeMessage();
            TPNSZ_Msg.Packet_Size = buffer.Length;
            TPNSZ_Msg.MsgType = NetworkHandle.Messages.MsgTypes.Status;


            foreach (DictionaryEntry con in Lists.ConnectionList)
            {
                try
                {
                    NetworkHandle .Client  connection = (NetworkHandle.Client)Lists.ConnectionList[con.Key]; 
                    SendPacket(connection.client  , TPNSZ_Msg.GetByte(), buffer);
                }
                catch
                {
                } 
            }
        }

        public void UploadToServer(int id, string ToName, string msg)
        {
            byte[] buffer;


            Cht_Msg = new NetworkHandle.Messages.ChatMessage();
            Cht_Msg.ID = id;
            Cht_Msg.Msg = msg;
            buffer = Cht_Msg.GetByte();

            TPNSZ_Msg = new NetworkHandle.Messages.TypeAndSizeMessage();
            TPNSZ_Msg.Packet_Size = buffer.Length;
            TPNSZ_Msg.MsgType = NetworkHandle.Messages.MsgTypes.Chat;

            NetworkHandle.ZeylandServer.AddMessage(NetworkHandle.MyInfo.MyUserName, ToName, TPNSZ_Msg.GetByte (), buffer);
        }
    }


}
