﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Data; 
 
namespace NetworkHandle
{
    public class UplodaedMessageHandle
    {
        public delegate void UplodedMessageHandler(string sender, Messages.ChatMessage chtmsg);
        public event UplodedMessageHandler ChatMessageReceived; 

        private Messages.TypeAndSizeMessage TPNSZ_msg;
        private Messages.ChatMessage Cht_Msg;
        private Messages.MessageContainer msgcontainer; 
        public UplodaedMessageHandle()
        {
        } 
        public void StartCheckForMessages()
        {
            while (true)
            {
                try
                {
                    string sendername;
                    byte[] tpns_byte;
                    byte[] msg_byte;

                    DataTable dt = new DataTable();
                    dt = ZeylandServer.GetMessages(MyInfo.MyUserName);

                    foreach (DataRow dr in dt.Rows)
                    {
                        sendername = dr["Sender"].ToString();
                        tpns_byte = (byte[])dr["TypeAndSize"];
                        msg_byte = (byte[])dr["Message"];
                        TPNSZ_msg = new NetworkHandle.Messages.TypeAndSizeMessage(tpns_byte);
                        if (TPNSZ_msg.MsgType == NetworkHandle.Messages.MsgTypes.Chat)
                        {
                            Cht_Msg = new NetworkHandle.Messages.ChatMessage(msg_byte);
                            if (Lists.MessageContainers.ContainsKey(sendername))
                            {
                                msgcontainer = (Messages.MessageContainer)Lists.MessageContainers[sendername];
                                msgcontainer.AddNew(sendername, Cht_Msg);
                            }
                            else
                            {
                                msgcontainer = new NetworkHandle.Messages.MessageContainer();
                                msgcontainer.AddNew(sendername, Cht_Msg);
                                Lists.MessageContainers.Add(sendername ,  msgcontainer ); 
                            }

                            if (ChatMessageReceived != null)
                            {
                                ChatMessageReceived(sendername, Cht_Msg); 
                            } 
                        }
                        else if (TPNSZ_msg.MsgType == NetworkHandle.Messages.MsgTypes.Request)
                        {
                            //TODO  Add your code here..
                            throw new NotImplementedException();
                        }
                    }
                }
                catch
                {
                }
                Thread.Sleep(2000); 
            }
        }

    }
}
