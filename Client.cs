﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading; 
using System.Collections; 

namespace NetworkHandle
{
    public class Client
    {
        private Messages.AuthenticationMessage Auth_msg;
        private Messages.TypeAndSizeMessage Tpnsz_msg; 
        private Messages.ChatMessage  Cht_msg ;
        private Messages.MessageContainer msgcontainer; 
        public  TcpClient client;
        public  Thread ThreadReceiveData;
        public  SendAndReceive SendReceiveHandle;
        public delegate void ClientHandler(Client  sender);
        public delegate void ChatMessageReceivedHandler(Client sender, Messages.ChatMessage cht_msg);
        public static event ClientHandler Connected;
        public  event ClientHandler Sent;
        public event ChatMessageReceivedHandler ChatMessageReceived; 
        public static  event ClientHandler StatusChanged;
        public event ClientHandler Disposed; 
       
        private string userName; 
        public string UserName
        {
            get { return userName; }
            set { userName = value; } 
        }

        private MyStatus  status;
        public MyStatus Status
        {
            get { return status; }
            set
            {
                status = value;
                if (StatusChanged != null)
                {
                    StatusChanged(this);
                }
            }
        } 

        public Client(TcpClient Client_Connection, bool CalledFromServer)
        {
            try
            {
                SendReceiveHandle = new SendAndReceive();
                client = Client_Connection;

                if (CalledFromServer)
                {
                    Auth_msg = SendReceiveHandle.Receive_Authentication_Message(this);
                    UserName = Auth_msg.UserName;
                    Status  = Auth_msg.Status;

                    SendReceiveHandle.Send_Authentication_Message(client, MyInfo.MyUserName, NetworkHandle.Messages.MsgTypes.Authentication, MyInfo.MyCurrentStatus);
                }
                else
                {
                    SendReceiveHandle.Send_Authentication_Message(client, MyInfo.MyUserName, NetworkHandle.Messages.MsgTypes.Authentication, MyInfo.MyCurrentStatus);

                    Auth_msg = SendReceiveHandle.Receive_Authentication_Message(this);
                    UserName = Auth_msg.UserName;
                    Status  = Auth_msg.Status;
                }

                Lists.ConnectionList.Add(UserName, this); 

                ThreadReceiveData = new Thread(new ThreadStart(ReceiveData));
                ThreadReceiveData.Priority = ThreadPriority.Lowest;
                ThreadReceiveData.Start();

                if (Connected != null)
                {
                    Connected(this);
                }
            }
            catch
            {
                Dispose(); 
            }
        }
        private void ReceiveData()
        {           
            while (true)
            {
                Tpnsz_msg  = SendReceiveHandle.ReceiveUpCommingMessageTypeAndSize(this);
                if (Tpnsz_msg.MsgType == NetworkHandle.Messages.MsgTypes.Chat)
                {
                    Cht_msg = SendReceiveHandle.ReceiveChatMessage(this, Tpnsz_msg.Packet_Size);
                    if (Lists.MessageContainers.ContainsKey(userName))
                    {
                        msgcontainer = (Messages.MessageContainer)Lists.MessageContainers[userName];
                        msgcontainer.AddNew(userName, Cht_msg);
                    }
                    else
                    {
                        msgcontainer = new NetworkHandle.Messages.MessageContainer();
                        msgcontainer.AddNew(userName, Cht_msg);
                        Lists.MessageContainers.Add(userName, msgcontainer); 
                    } 
                    if (ChatMessageReceived  != null)
                    {
                        ChatMessageReceived(this, Cht_msg); 
                    }
                }
                else if (Tpnsz_msg.MsgType == NetworkHandle.Messages.MsgTypes.Status)
                {
                    Status = SendReceiveHandle.Recive_Status_Message(this, Tpnsz_msg.Packet_Size).Status;
                }
            }                      
        }
        public void  SendChatMessage(int ID , string msg ) 
        {
            SendReceiveHandle.Send_Chat_Message(client, ID, msg);

            if (Sent != null)
            {
                Sent(this );
            } 
        }

        public void Dispose()
        {
           
            client.Close();
            client = null;
            Lists.ConnectionList.Remove(UserName);
            Status = MyStatus.OffLine; 
            if (Disposed != null)
            {
                Disposed(this); 
            } 
            this.ThreadReceiveData.Abort();

        }
    }
}
