﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO; 

namespace NetworkHandle
{
   public static  class ImageHandle
    {

#region convert image to less less size.
        /// <summary>
        /// Method for getting the encoder info out of a mime type.
        /// </summary>
        /// <param name="mimeType">The mime type to search for.</param>
        /// <returns>The <see cref="ImageCodecInfo" /> for the supplied mime type. If the mime type is not found, null is returned.</returns>
        private static System.Drawing.Imaging.ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int i;

            System.Drawing.Imaging.ImageCodecInfo[] encoders;
            encoders = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders();
            for (i = 0; i < encoders.Length; ++i)
            {
                if (encoders[i].MimeType == mimeType)
                    return encoders[i];
            }
            return null;
        }
        /// <summary>
        /// Image processing reduces the file quality to level 25 (of 100) and stores the new image.
        /// </summary>
        /// <param name="inputFilename">The image to process.</param>
        /// <param name="outputFilename">Filename, where the processed image should be saved.</param>
        public static Image ReduceImageQuality(Image inputImg)
        {
            // Load the image from the file.
            Image inputImage = inputImg;  
            // Get an ImageCodecInfo object that represents the JPEG codec.
            System.Drawing.Imaging.ImageCodecInfo imageCodecInfo = GetEncoderInfo("image/jpeg");
            // Get the image encoder Guid for the quality property.
            System.Drawing.Imaging.Encoder qualityParamIdentifier = System.Drawing.Imaging.Encoder.Quality;
            // Initialize the encoder parameters array.
            System.Drawing.Imaging.EncoderParameters encoderParameters = new System.Drawing.Imaging.EncoderParameters(1);
            // Define a new EncoderParameter for the quality parameter and set it to quality level 25 (of 100)
            System.Drawing.Imaging.EncoderParameter qualityParam = new System.Drawing.Imaging.EncoderParameter(qualityParamIdentifier, 25L);
            // Add the quality parameter to the encoder.
            encoderParameters.Param[0] = qualityParam;
            //Create Memeory stream instance
            MemoryStream ms = new MemoryStream();
            // Save the bitmap.
            inputImage.Save(ms, imageCodecInfo, encoderParameters);
            Image imgresult  = Image.FromStream(ms);
            return imgresult;
        }
#endregion 
 #region convert images to byte arrays

        public static  byte[] ImageToByteArray(System.Drawing.Image Img ) 
        {
            MemoryStream ms = new MemoryStream();
            Img.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray(); 
        }
        public static Image ByteArrayToImage(byte[] bytearray)
        {
            MemoryStream ms = new MemoryStream(bytearray);
            Image returnImage = Image.FromStream(ms);
            return returnImage; 
        } 

        #endregion 

        public static byte[] ReduceImageQualityAndConvertToAByteArray(Image img)
        {
            Image result = ReduceImageQuality(img);
            byte[] bytearray = ImageToByteArray(result);
            return bytearray; 
        }
    }
}
