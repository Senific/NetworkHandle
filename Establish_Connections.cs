﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;


namespace NetworkHandle
{

    public class Establish_Connections
    {

        /// <summary>
        /// This method Create a loop for attempt to establish connections with each clients who has not been already connected.
        /// </summary>
        public void ConnectingLoop()
        {
            while (true)
            {
                try
                {
 
                    foreach (DictionaryEntry contact in Lists.ContactList)
                    {
                        ZeylandServer.UpdateEndpointOfThisContact(contact.Key.ToString()); 
                    }

                    foreach (DictionaryEntry contact in Lists.ContactList)
                    {
                        if (!Lists.ConnectionList.ContainsKey(contact.Key) && Lists.IPEndpointList .ContainsKey (contact.Key ))  
                        {
                            if (Lists.IPEndpointList[contact.Key] != null)
                            {
                                try
                                {
                                    IPEndPoint ipep = (IPEndPoint)Lists.IPEndpointList[contact.Key]; 
                                    TcpClient remoteclient = new TcpClient();
                                    remoteclient.Connect(ipep);
                                    Client client = new Client(remoteclient, false);
                                }
                                catch
                                {
                                }
                            }
                        }
                    }
                }
                catch
                {
                }

                Thread.Sleep(2000);
            }

        }



    }
}
