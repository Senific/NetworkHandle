﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net ;
using System.Collections;
using System.Data;

namespace NetworkHandle
{
    public static class ZeylandServer
    {
        private static ZeylandAddresses.Addresses addresses = new NetworkHandle.ZeylandAddresses.Addresses();
        private static ZeylandContacts.Contacts contacts = new NetworkHandle.ZeylandContacts.Contacts();
        private static ZeylandMessages.MessageList messagelist = new NetworkHandle.ZeylandMessages.MessageList();

        public static void AssigneIPEndpointToTheZeylandServer(IPEndPoint ipep)
        {
            addresses.Add(MyInfo.MyUserName.ToLower(), ipep.Address.ToString(), ipep.Port);
        }
        public static void GetContactList()
        {
            DataSet ds;
            ds = contacts.GetContactList(NetworkHandle.MyInfo.MyUserName);
            foreach (DataRow contact in ds.Tables[0].Rows)
            {
                Lists.ContactList.Add(contact[0].ToString().ToLower(), null); 
            }
        }
        public static void UpdateEndpointOfThisContact(string UserName)
        {
            UserName = UserName.ToLower();
            string[] arr = addresses.GetAddress(UserName);
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(arr[0]), int.Parse(arr[1]));

            if (Lists.IPEndpointList.ContainsKey(UserName ))
            {
                Lists.IPEndpointList[UserName] = ipep; 
            }
            else
            {
                Lists.IPEndpointList.Add(UserName, ipep);
            }
        }
        public static void GetEndPointForAllContacts()
        {
            foreach (DictionaryEntry contact in Lists.ContactList)
            {
                string[] arr = addresses.GetAddress(contact.Key.ToString());
                IPEndPoint ipep = new IPEndPoint
                (
                  IPAddress.Parse(arr[0]), int.Parse(arr[1])
                 );

                if (Lists.ContactList.ContainsKey(contact.Key.ToString()))
                {
                    Lists.ContactList[contact.Key.ToString()] = ipep;
                }
            }
        }
        public static bool GetAuthenticationResult(string UserName, string Password)
        {
            return contacts.Authenticate_UserNameAndPassword(UserName, Password);
        }
        public static bool NewUser(string UserName, string Password
            , string name, string FirstName , string LastName 
            , string MobilePhone, string HomePhone
            , string OfficePhone, string Country, string state
            , string City, string timezone, string website
            , string gender, DateTime birthdate, string language
            , string aboutme, byte[] pic)
        {
            return contacts.NewUser(UserName, Password, name, FirstName, LastName, MobilePhone, HomePhone
                   , OfficePhone, Country, state, City, timezone, website,
                   gender, birthdate, language, aboutme, pic);
        }
        public static DataTable  Search(string username, int AgeLessThan, int AgeGreaterThan,
            string Country, string Gender)
        {
            return contacts.SerachContacts(username, AgeLessThan, AgeGreaterThan, Country, Gender).Tables[0]; 
        }
        public static bool AddContact( string contactusername)
        {
            try
            {
               return contacts .AddContact(MyInfo.MyUserName, contactusername);
                
            }
            catch
            {
                return false; 
            } 
        }
        public static bool AddMessage(string sender , string receiver , byte[] typeandsize , byte[] msg)
        {
            try
            { 
                return messagelist.AddToMessageList(sender, receiver, typeandsize, msg); 
            }
            catch
            {
                return false;
            }  
        }
        public static DataTable GetMessages(string username)
        {
          return messagelist.GetMyMessageList(username).Tables[0]; 
        }
    }
     
}
