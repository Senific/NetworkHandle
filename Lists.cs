﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading;
using System.Net; 

namespace NetworkHandle
{
    public static class Lists
    {

        /// <summary>
        /// Contains Key = UserName  / Value = null ; 
        /// </summary> 
        /// 
        private static NetworkHandle.ObservableHashtable contactList = new NetworkHandle.ObservableHashtable();
        public static NetworkHandle.ObservableHashtable ContactList
        {
            get
            {
                return contactList;
            }
            set
            {
                contactList = value;
            }
        }

        /// <summary>
        /// Contians Key = UserName / Value = IPendpoint
        /// </summary>
        private static NetworkHandle.ObservableHashtable IpendpointList = new ObservableHashtable();
        public static NetworkHandle.ObservableHashtable IPEndpointList
        {
            get { return IpendpointList; }
            set { IpendpointList = value; } 
        }

        /// <summary>
        /// Contains Key = UserName / Value = Connection ( Client class Instance )  
        /// </summary>
        private static NetworkHandle.ObservableHashtable connectionList = new NetworkHandle.ObservableHashtable();
        public static NetworkHandle.ObservableHashtable ConnectionList
        {
            get
            {
                return connectionList;
            }
            set
            {
                connectionList = value;
            }
        }

        /// <summary>
        /// Contains  Key = Key  / value =  Threed ; 
        /// </summary>
        private static NetworkHandle.ObservableHashtable staticThreeds = new NetworkHandle.ObservableHashtable();
        public static NetworkHandle.ObservableHashtable StaticThreeds
        {
            get
            {
                return staticThreeds;
            }
            set
            {
                staticThreeds = value;
            }

        }

        /// <summary>
        /// Contains Key = UserName  / Value = ChatViewer instance 
        /// </summary>
        private static NetworkHandle.ObservableHashtable chatviewers = new NetworkHandle.ObservableHashtable();
        public static NetworkHandle.ObservableHashtable ChatViewers
        {
            get
            {
                return chatviewers;
            }
            set
            {
                chatviewers = value;
            }
        }

        /// <summary>
        /// Contains Key = UserName  / value = MessageContainer
        /// </summary>
        private static NetworkHandle.ObservableHashtable messageContainers = new ObservableHashtable();
        public static NetworkHandle.ObservableHashtable  MessageContainers
        {
            get
            {
                return messageContainers ; 
            }
            set
            { 
                messageContainers = value; 
            } 
        } 
      
    }
}