﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections;

namespace NetworkHandle
{
    public static class MyInfo
    {

        private static string myUserName;
        public static string MyUserName
        {
            get { return myUserName; }
            set { myUserName = value; }
        }

        private static MyStatus myCurrentStatus;
        public static MyStatus MyCurrentStatus
        {
            get { return myCurrentStatus; }
            set
            {
                    SendAndReceive sendAndReceive = new SendAndReceive();
                    sendAndReceive.Send_Status_Message_ForAllConnections(value);
            }
        }

   
       
    }
}