﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace NetworkHandle
{
    public class Listen
    {

        public static TcpListener Listener;
        private TcpClient Accepted_Client;



        public void Start_Listening()
        {

            Listener = new TcpListener(0);
            Listener.Start();
            IPEndPoint ipep = (IPEndPoint)Listener.Server.LocalEndPoint;
            IPHostEntry ipaddresses = Dns.GetHostByName(Dns.GetHostName());
            ipep.Address = ipaddresses.AddressList[0];
            ZeylandServer.AssigneIPEndpointToTheZeylandServer(ipep);
            while (true)
            {
                try
                {
                    Accepted_Client = Listener.AcceptTcpClient();
                    Client client = new Client(Accepted_Client, true);
                }
                catch
                {
                    break;
                }

            }
        }
    }
}