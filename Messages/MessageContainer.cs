﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkHandle.Messages
{
   public  class MessageContainer
     {
         private MessageHandle.MessageDisplay msgdisplay = new MessageHandle.MessageDisplay();

         private string docText = null;
         public string DocText
         {
             get { return docText; }
             set { docText = value; }
         }

         public void AddNew(string From, ChatMessage  msg)
         {
             DocText = DocText + msgdisplay.ConvertMessageToOutPut(From  , msg.Msg );
         }
    }
}
