﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net; 

namespace NetworkHandle.Messages
{
   public  class TypeAndSizeMessage
    {
        byte[] byteArray; 
        byte[] newbyte;
        int place; 

        private int packet_size;
        public int Packet_Size
        {
            get { return packet_size; }
            set { packet_size = value; } 
        }

        private MsgTypes msgType;
        public MsgTypes  MsgType
        {
            get { return msgType; }
            set { msgType = value; } 
        }


        public TypeAndSizeMessage()
        {
        } 
        public TypeAndSizeMessage(byte[] buffer )
        {
            Packet_Size = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, 0));
            MsgType = (MsgTypes)IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, BinaryDataTypeSizes.int_DataTypeSize));
 
        }

        public byte[] GetByte()
        {
            place = 0;
            byte[] byteOne;
            byte[] byteTwo;


            byteOne  = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(packet_size));
            place += byteOne.Length; 
            byteTwo  = BitConverter.GetBytes(IPAddress.HostToNetworkOrder((int)msgType));
            place += byteTwo.Length;

            byteArray = new byte[place];
            place = 0; 
            Buffer.BlockCopy(byteOne , 0, byteArray, 0, byteOne.Length );
            place += byteOne.Length; 
            Buffer.BlockCopy(byteTwo , 0, byteArray, place, byteTwo.Length );
            place += byteTwo.Length;

            return byteArray; 

        }
    }
}
