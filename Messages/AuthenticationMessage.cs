﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net; 

namespace NetworkHandle.Messages
{
  public   class AuthenticationMessage
    { 
        int place; 
     
        private  byte[] byteArray;
       public byte[] ByteArray
       {
           get { return byteArray; }
           set { byteArray = value; } 
       } 

       

        private string userName;
        public string UserName
        {
            get { return userName; }
            set { userName = value; }  
        }

        private int userNameLength;
        private int UserNameLength
        {
            get { return userNameLength; }
            set { userNameLength = value; } 
        } 

        private MyStatus   status;
        public MyStatus Status
        {
            get { return status; }
            set { status = value; } 
        }

        

        public AuthenticationMessage()
        {

        } 
        public AuthenticationMessage(byte[] buffer)
        {
            place = 0;

            UserNameLength = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, 0));
            place += BinaryDataTypeSizes.int_DataTypeSize; 

            UserName = Encoding.ASCII.GetString(buffer , place , UserNameLength );
            place += UserNameLength;

            Status = (MyStatus ) IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, place));
            place += BinaryDataTypeSizes.int_DataTypeSize; 

        } 

        public byte[] GetByte()
        {
            place = 0;
            byte[] byteOne;
            byte[] byteTwo;
            byte[] byteThree;

            byteOne  = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(UserName.Length));
            place += byteOne.Length; 
            byteTwo  = Encoding.ASCII.GetBytes(UserName);
            place += byteTwo.Length; 
            byteThree  = BitConverter.GetBytes(IPAddress.HostToNetworkOrder((int)Status));
            place += byteThree.Length; 

            ByteArray = new byte[place];
            place = 0;
            Buffer.BlockCopy(byteOne, 0, ByteArray, 0, byteOne.Length);
            place += byteOne.Length;
            Buffer.BlockCopy(byteTwo, 0, ByteArray, place, byteTwo.Length);
            place += byteTwo.Length;
            Buffer.BlockCopy(byteThree, 0, ByteArray, place, byteThree.Length);
            place += byteThree.Length; 
            return ByteArray ; 
        }

    }
}
