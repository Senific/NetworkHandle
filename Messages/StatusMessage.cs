﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net; 

namespace NetworkHandle.Messages
{
  public   class StatusMessage
    {
     
        int place;

        private byte[] byteArray;
        public byte[] ByteArray
        {
            get { return byteArray; }
            set { byteArray = value; }
        }

        private MyStatus status;
        public MyStatus Status
        {
            get { return status; }
            set { status = value; }
        }
        
        public StatusMessage () 
        {

        } 
        public  StatusMessage(byte[] buffer)
        {
            place = 0;

            Status = (MyStatus ) IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, place));
            place += BinaryDataTypeSizes.int_DataTypeSize; 

        } 

        public byte[] GetByte()
        {
            place = 0;
            byte[] byteOne;
         
            byteOne  = BitConverter.GetBytes(IPAddress.HostToNetworkOrder((int)Status));
            place += byteOne.Length;

            ByteArray = new byte[place];
            place = 0; 
            Buffer.BlockCopy(byteOne  ,0, ByteArray , 0 , byteOne.Length );
            place += byteOne.Length; 

            return ByteArray ; 
        }
    }
}
