﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net; 

namespace NetworkHandle.Messages
{
   public  class ChatMessage
    {
        int place;


        private byte[] byteArray;
        public byte[] ByteArray
        {
            get { return byteArray; }
            set { byteArray = value; }
        }


        private long  iD;
        public  long  ID
        {
            get { return iD; }
            set { iD = value; }
        }

        private string msg;
        public string Msg
        {
            get { return msg; }
            set { msg = value; }
        }

        private int msgLength;
        private int MsgLength
        {
            get { return msgLength; }
            set { msgLength = value; }
        } 


        public ChatMessage()
        {
        }

        public ChatMessage(byte[] buffer)
        {
            place = 0;

            ID = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, 0));
            place += BinaryDataTypeSizes.long_DataTypesize; 

            MsgLength  = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(buffer, place ));
            place += BinaryDataTypeSizes.int_DataTypeSize;

            Msg = Encoding.ASCII.GetString(buffer, place, MsgLength);
            place += MsgLength; 
            
        }

        public byte[] GetByte()
        {
            place = 0;
            byte[] byteOne;
            byte[] byteTwo;
            byte[] byteThree;

            byteOne = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(ID));
            place += byteOne.Length;
            byteTwo = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(msg.Length));
            place += byteTwo.Length;
            byteThree = Encoding.ASCII.GetBytes(msg);
            place += byteThree.Length;

            ByteArray = new byte[place];
            place = 0;
            Buffer.BlockCopy(byteOne, 0, ByteArray, 0, byteOne.Length);
            place += byteOne.Length;
            Buffer.BlockCopy(byteTwo, 0, ByteArray, place, byteTwo.Length);
            place += byteTwo.Length;
            Buffer.BlockCopy(byteThree, 0, ByteArray, place, byteThree.Length);
            place += byteThree.Length;

            return ByteArray;
        }

    }
}
